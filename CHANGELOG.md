# Change Log

## 0.0.9 - 2020-02-20

* Minor bug fixes and code refactoring

## 0.0.8 - 2019-12-31

* Added extra scan in resources section for IntelliSense
* Added code-completion for port behaviors
* Other minor enhancements to IntelliSense

## 0.0.7 - 2019-12-24

* Major refactor on the syntax highlighting
* Added support of "Go To Definition" for LISA+ protocols
* Performance improvement on indexing
* Minor bug fixes

## 0.0.6 - 2019-12-23

* Added initial support for component connections visualization
* Fixed the bug that cannot extract the information of certain components

## 0.0.5

* Added support of "Go To Definition" for LISA+ components

## 0.0.4

* Further enhancements on the IntelliSense for ports with extra information

## 0.0.3

* Added code-completion of ports, protocols, compositions and components
* Added "LISA+: Scan Workspace" command
* Added support for sgrepo files
* Added support for sgproj files
* Minor bug fixes

## 0.0.2

* Enhancement on the code-completion
* Minor bug fixes

## 0.0.1

Initial release of LISA+ Language Support
