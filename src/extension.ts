import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';


let lisa_files: vscode.Uri[] = [];
let component_ports = new Map();
let component_definitions = new Map();
let protocol_definitions = new Map();
let protocol_behaviors = new Map();
let instances_map = new Map();

class LISADefinitionProvider implements vscode.DefinitionProvider {
	public provideDefinition(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken)
	{
		let editor = vscode.window.activeTextEditor;
		if (editor)
		{
			let cursor = editor.selection.start;
			let word_range = document.getWordRangeAtPosition(cursor);
			let word = document.getText(word_range);
			if (component_definitions.has(word))
			{
				let definitions = component_definitions.get(word);

				return definitions[0];
			}
			else if (protocol_definitions.has(word))
			{
				let definitions = protocol_definitions.get(word);
				return definitions[0];
			}
		}

		return null;
	}
}

function extractBracket(text: string, pos: number, bracket: string): string
{
	let content: string = "";
	let brackets = null;
	let bracket_counter = 0;
	let recording = false;
	if (bracket == "paren") // ()
		brackets = ['(', ')'];
	else if (bracket == "square")
		brackets = ['[', ']'];
	else if (bracket == "curly")
		brackets = ['{', '}'];
	else
		return content;

	for (let i=pos; i<text.length; i++)
	{
		if(text.charAt(i) == brackets[0])
		{
			if(bracket_counter == 0)
			{
				recording = true;
				bracket_counter++;
				continue;
			}
			bracket_counter++;
			content += text.charAt(i);
		}
		else if(text.charAt(i) == brackets[1])
		{
			bracket_counter--;
			if(bracket_counter == 0)
			{
				recording = false;
				break;
			}
			content += text.charAt(i);
		}
		else if(recording)
		{
			content += text.charAt(i);
		}
	}

	return content;
}

function stripComments(text: string): string
{
	let text_processed = "";
	let lines = text.split('\n');
	for(let line of lines)
	{
		let line_trimmed = line.trim();
		if(line_trimmed.startsWith("*") || line_trimmed.startsWith("/"))
			text_processed += "\n";
		else
			text_processed += line + "\n";
	}
	return text_processed;
}

function getDocumentText(document: vscode.TextDocument)
{
	let text: string = "";

	let first_line = document.lineAt(0);
	let last_line = document.lineAt(document.lineCount - 1);
	let doc_range = new vscode.Range(first_line.range.start, last_line.range.end);
	text = document.getText(doc_range);

	return text;
}

function getCurrentComponent(text: string) : string
{
	let current_component = "";
	let current_component_regexp = new RegExp("component\\s+([\\w\\d]+)\\s*\\{(.*?)^\\}", "gms");
	let matches = text.matchAll(current_component_regexp); // This is not a very reliable way of extracting the contents with {}
	for (let match of matches) // Extracting components in the lisa files
	{
		current_component = match[1];
		break;
	}
	return current_component;
}

function constructInstancesMap(text: string)
{
	let composition_regexp = new RegExp("composition\\s*{(.*?)}", "gms");
	let composition = composition_regexp.exec(text);
	let component_regexp = new RegExp('([\\w\\d]+)\\s*:\\s*([\\w\\d]+)\\(.*?\\);', 'gms');
	instances_map.clear();
	if (composition)
	{
		let matches = composition[0].matchAll(component_regexp);
		for (let match of matches)
		{
			instances_map.set(match[1], match[2]);
		}
	}
}

function extractResources(text: string)
{
	let resources_regexp = new RegExp("resources\\s*{", "gms");
	let pos = text.search(resources_regexp);
	let resources_text = extractBracket(text, pos, "curly");

	let register_regexp = new RegExp('REGISTER\\s*\\{.*?\\}\\s*(\\w+);', 'gms');
	let parameter_regexp = new RegExp('PARAMETER\\s*\\{.*?\\}\\s*(\\w+);', 'gms');

	let resourcesCompletion = [];
	if (resources_text)
	{
		let matches = resources_text.matchAll(register_regexp);
		for (let match of matches)
		{
			resourcesCompletion.push(new vscode.CompletionItem(match[1], vscode.CompletionItemKind.Variable));
		}
		matches = resources_text.matchAll(parameter_regexp);
		for (let match of matches)
		{
			resourcesCompletion.push(new vscode.CompletionItem(match[1], vscode.CompletionItemKind.TypeParameter));
		}
	}

	return resourcesCompletion;
}

export function activate(context: vscode.ExtensionContext): void {
	let provider_variables = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			let text = getDocumentText(document);
			constructInstancesMap(text);

			let simpleCompletion_compositions = [];
			for (let instance of instances_map.keys())
			{
				let completionItem = new vscode.CompletionItem(instance);
				completionItem.kind = vscode.CompletionItemKind.Variable;
				simpleCompletion_compositions.push(completionItem);
			}
			let components = Array.from(new Set(instances_map.values()));
			for (let component of components)
			{
				let completionItem = new vscode.CompletionItem(component);
				completionItem.kind = vscode.CompletionItemKind.Module;
				simpleCompletion_compositions.push(completionItem);
			}

			return simpleCompletion_compositions;
		}
	});

	let provider_ports = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			let text = getDocumentText(document);
			let port_regexp = new RegExp("^\\s*(internal)?\\s*(master|slave)\\s*(addressable)?\\s*port\\s*<(.*?)>\\s*([\\w\\d]+)", "gms");
			let protocols: string[] = [];
			let self_ports = [];
			let matches = text.matchAll(port_regexp);
			for (let match of matches)
			{
				self_ports.push(Array.from(match));
				if (!protocols.includes(match[4]))
				{
					protocols.push(match[4]);
				}
			}

			let simpleCompletion_protocols = [];
			for (let protocol of protocols)
			{
				simpleCompletion_protocols.push(new vscode.CompletionItem(protocol, vscode.CompletionItemKind.Class));
			}
			let simpleCompletion_ports = [];
			let linePrefix = document.lineAt(position).text.substr(0, position.character);
			let trigger_regexp = new RegExp('self\\.(\\w+)\\.$');
			let implemented_trigger_regexp = new RegExp('self\\.(\\w+)\\.(\\w+)\\.$');
			let behavior_trigger_regexp = new RegExp('self\\.(\\w+)\\.(\\w+)\\s*\\($');
			if (linePrefix.endsWith('self.')) {
				for (let port of self_ports)
				{
					let item = new vscode.CompletionItem(port[5], vscode.CompletionItemKind.Field);

					item.documentation = new vscode.MarkdownString();
					item.documentation.appendMarkdown("* **Internal:** " + ((port[1] == "internal")? "True":"False") + "\n");
					item.documentation.appendMarkdown("* **Type:** " + port[2] + "\n");
					item.documentation.appendMarkdown("* **Addressable:** " + ((port[3] == "addressable")? "True":"False") + "\n");
					item.documentation.appendMarkdown("* **Protocol:** " + port[4] + "\n");

					simpleCompletion_ports.push(item);
				}
				return simpleCompletion_ports;
			}
			else if(trigger_regexp.test(linePrefix))
			{
				let port_name = linePrefix.split('.')[1];
				for(let port of self_ports)
				{
					if (port_name == port[5])
					{
						let behaviors = protocol_behaviors.get(port[4]);
						for (let behavior of behaviors)
						{
							let item = new vscode.CompletionItem(behavior[3], vscode.CompletionItemKind.Function);
							item.documentation = new vscode.MarkdownString();
							if(behavior[1])
								item.documentation.appendMarkdown(behavior[1] + ' ');
							item.documentation.appendMarkdown(behavior[2] + ' ' + behavior[3] + '(' + behavior[4] + ')');
							if (behavior[5])
								item.documentation.appendMarkdown(' : ' + behavior[5]);
							item.documentation.appendMarkdown('\n');
							simpleCompletion_ports.push(item);
						}
						break;
					}
				}
				return simpleCompletion_ports;
			}
			else if(implemented_trigger_regexp.test(linePrefix))
			{
				let port_name = linePrefix.split('.')[1];
				let potential_behavior = linePrefix.split('.')[2];
				for(let port of self_ports)
				{
					if (port_name == port[5])
					{
						let behaviors = protocol_behaviors.get(port[4]);
						for (let behavior of behaviors)
						{
							if(behavior[3] == potential_behavior)
							{
								simpleCompletion_ports.push(new vscode.CompletionItem("implemented()", vscode.CompletionItemKind.Function));
								return simpleCompletion_ports;
							}
						}
					}
				}
			}
			else
			{
				let words = linePrefix.slice(0, -1).trim().split(' ');
				let instance = words[words.length - 1];
				// console.log("DEBUG: instance - " + instance);
				if(instances_map.has(instance))
				{
					if(component_ports.has(instances_map.get(instance)))
					{
						// console.log("DEBUG: component - " + instances_map.get(instance));
						let ports = component_ports.get(instances_map.get(instance));
						for(let port of ports)
						{
							let item = new vscode.CompletionItem(port[5], vscode.CompletionItemKind.Field);

							item.documentation = new vscode.MarkdownString();
							item.documentation.appendMarkdown("* **Internal:** " + ((port[1] == "internal")? "True":"False") + "\n");
							item.documentation.appendMarkdown("* **Type:** " + port[2] + "\n");
							item.documentation.appendMarkdown("* **Addressable:** " + ((port[3] == "addressable")? "True":"False") + "\n");
							item.documentation.appendMarkdown("* **Protocol:** " + port[4] + "\n");
							// console.log("DEBUG: port - " + port[5]);
							// console.log("DEBUG:  " + item.documentation);
							simpleCompletion_ports.push(item);
						}
						return simpleCompletion_ports;
					}
				}
			}

			return simpleCompletion_protocols;
		}
	}, '.'
	);

	let provider_sections = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			// Simple completion items for section keywords
			let sections = ["component", "includes", "properties", "resources", "composition", "connection", "behavior"];
			let simpleCompletion_sections = [];
			for (let section of sections) {
				simpleCompletion_sections.push(new vscode.CompletionItem(section));
			}
			return simpleCompletion_sections;
		}
	});

	let provider_types = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			// Simple completion items for type keywords
			let types = ["master", "slave", "internal", "extern", "enum", "union", "class", "struct", "port", "self"];
			let simpleCompletion_types = [];
			for (let type of types) {
				simpleCompletion_types.push(new vscode.CompletionItem(type, vscode.CompletionItemKind.Class));
			}

			return simpleCompletion_types;
		}
	});

	let provider_native_types = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			// Simple completion items for type keywords
			let types = ["int8_t", "int16_t", "int32_t", "int64_t", "uint8_t", "uint16_t", "uint32_t", "uint64_t", "int", "long", "short", "bool", "char", "signed", "unsigned", "float", "double"];
			let simpleCompletion_types = [];
			for (let type of types) {
				simpleCompletion_types.push(new vscode.CompletionItem(type, vscode.CompletionItemKind.Unit));
			}

			return simpleCompletion_types;
		}
	});

	let provider_resources = vscode.languages.registerCompletionItemProvider('lisa', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
			// Command completion for PARAMETER
			const commandCompletion_params = new vscode.CompletionItem('PARAMETER');
			commandCompletion_params.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_params.insertText = 'PARAMETER { description(""), type(""), default(0), min(0), max(0xFFFF), runtime(false) } parameter_name;';
			// commandCompletion_params.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

			// Command completion for REGISTER
			const commandCompletion_register = new vscode.CompletionItem('REGISTER');
			commandCompletion_register.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_register.insertText = 'REGISTER { description(""), type(uint), bitwidth(32), read_function("debug_read"), write_function("debug_write"), reg_number(0x..) } register_name;';
			// commandCompletion_register.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

			// Command completion for MEMORY
			const commandCompletion_memory = new vscode.CompletionItem('MEMORY');
			commandCompletion_memory.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_memory.insertText = 'MEMORY { description(""), attribute(read_write),  mau_size(32), read_function(""), write_function("") } memory_name[x];';
			// commandCompletion_memory.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

			// Command completion for master
			const commandCompletion_master = new vscode.CompletionItem('master');
			commandCompletion_master.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_master.insertText = 'master port<> port_name;';
			// commandCompletion_master.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

			// Command completion for slave
			const commandCompletion_slave = new vscode.CompletionItem('slave');
			commandCompletion_slave.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_slave.insertText = 'slave port<> port_name;';
			// commandCompletion_slave.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

			// Command completion for port
			const commandCompletion_port = new vscode.CompletionItem('port');
			commandCompletion_port.kind = vscode.CompletionItemKind.Keyword;
			commandCompletion_port.insertText = 'port<> port_name;';

			let autocompletions = [
				commandCompletion_params,
				commandCompletion_register,
				commandCompletion_memory,
				commandCompletion_master,
				commandCompletion_slave,
				commandCompletion_port
			];

			let content = getDocumentText(document);
			content = stripComments(content);
			autocompletions = autocompletions.concat(extractResources(content));
			return autocompletions;
		}
	});

	context.subscriptions.push(provider_sections, provider_types, provider_native_types, provider_resources, provider_variables, provider_ports);

	const command_scan = "lisa.scan";
	const scan_handler = async () => {
		vscode.window.showInformationMessage("LISA+ Extension: Scanning workspace for IntelliSense ...");
		lisa_files = await vscode.workspace.findFiles('**/*.lisa');
		for (let file of lisa_files)
		{
			let content = fs.readFileSync(file.path, 'utf-8');
			content = stripComments(content);

			let protocol_regexp = new RegExp("protocol\\s+([\\w\\d]+)\\s*\\{(.*?)^\\s?\\}", "gms");
			let component_regexp = new RegExp("component\\s+([\\w\\d]+)\\s*\\{", "gms");
			let port_regexp = new RegExp("^\\s*(internal)?\\s*(master|slave)\\s*(addressable)?\\s*port\\s*<(.*?)>\\s*([\\w\\d]+)", "gms");
			let found_anything = false;

			let found;
			while(found = component_regexp.exec(content))
			{
				found_anything = true;

				let name = found[1];
				// Gather component definitions
				let lines = content.split('\n');
				let lineCount = -1;
				for(let line of lines)
				{
					lineCount += 1;
					let pos = line.search("component\\s+"+name);
					if(pos != -1) // Found the line
					{
						let position = new vscode.Position(lineCount, pos);
						let location = new vscode.Location(file, position);
						if(!component_definitions.has(name))
						{
							component_definitions.set(name, [location]);
						}
						else
						{
							let defs = component_definitions.get(name);
							defs.push(location);
							component_definitions.set(name, defs);
						}
						break;
					}
				}

				let text = extractBracket(content, found.index, "curly");
				let ports = text.matchAll(port_regexp);
				let ports_instances = [];
				for (let port of ports)
				{
					// let documentation = " * Port: " + port[5] + "\n"
					// documentation += " * Internal: " + ((port[1] == "internal")? "True":"False") + "\n";
					// documentation += " * Type: " + port[2] + "\n";
					// documentation += " * Addressable: " + ((port[3] == "addressable")? "True":"False") + "\n";
					// documentation += " * Protocol: " + port[4] + "\n";

					// group 1: internal, group 2: master/slave, group 3: addressable, group 4: protocol, group 5: port name
					ports_instances.push(Array.from(port));
				}
				component_ports.set(name, Array.from(ports_instances));
			}
			if(!found_anything)
			{
				let protocol_regexp = new RegExp("protocol\\s+([\\w\\d]+)\\s*\\{", "gms");
				let found;
				while(found = protocol_regexp.exec(content))
				{
					found_anything = true;

					let name = found[1];
					// Gather component definitions
					let lines = content.split('\n');
					let lineCount = -1;
					for(let line of lines)
					{
						lineCount += 1;
						let pos = line.search("protocol\\s+"+name);
						if(pos != -1) // Found the line
						{
							let position = new vscode.Position(lineCount, pos);
							let location = new vscode.Location(file, position);
							if(!protocol_definitions.has(name))
							{
								protocol_definitions.set(name, [location]);
							}
							else
							{
								let defs = protocol_definitions.get(name);
								defs.push(location);
								protocol_definitions.set(name, defs);
							}
							break;
						}
					}
					let body = extractBracket(content, found.index, "curly");
					let behavior_regex = new RegExp("(optional)?\\s*(behavior|behaviour)\\s*(\\w+)\\s*\\((.*?)\\)\\s*:?\\s*(.*?)[;\\{]+", "gms");
					let behaviors = body.matchAll(behavior_regex);
					for (let behavior of behaviors)
					{
						/*
						 * group 1 option
						 * group 2 behavior keyword
						 * group 3 behavior name
						 * group 4 behavior parameters
						 * group 5 return type
						 */
						if (!protocol_behaviors.has(name))
						{
							protocol_behaviors.set(name, [behavior]);
						}
						else
						{
							let defs = protocol_behaviors.get(name);
							defs.push(behavior);
							protocol_behaviors.set(name, defs);
						}
					}
				}
			}
		}
		vscode.window.showInformationMessage("LISA+ Extension: Scanning workspace for IntelliSense ... done");
		console.log("INFO: Workspace scan accomplished");
	};
	context.subscriptions.push(vscode.commands.registerCommand(command_scan, scan_handler));

	const command_visualize = "lisa.visualize";
	const visualize_handler = async () => {
		let instances_graphviz = new Map();
		let current_document = vscode.window.activeTextEditor;
		let text = "";
		if (current_document != undefined)
		{
			text = getDocumentText(current_document.document);
			text = stripComments(text);
		}
		constructInstancesMap(text);
		let current_component = getCurrentComponent(text);
		let gv_file_content = 'digraph G {\n'
			+ 'graph [labelloc="t" label="' + current_component + '" layout="dot" splines=true overlap=false ranksep=1 nodesep=0.5 rankdir="LR"];\n'
			+ 'ratio = auto;\n'
			+ '\n"self" [ shape=record ];';
		instances_map.forEach((value: string, key: string) => {
			if (component_ports.has(value))
			{
				let ports = component_ports.get(value);
				let code_block = '"' + key + '" '
				+ '[ style = "filled, solid" penwidth = 1 fillcolor = "white" fontname = "Arial" fontsize = 9 shape = none margin = 0 label =\n'
				+ '\t<<table border="1" cellborder="0" cellpadding="1" bgcolor="white">\n'
				+ '\t\t<tr><td bgcolor="#ADD8E6" align="center" colspan="2">' + key + '(' + value + ')</td></tr>\n';
				let masters = [];
				let slaves = [];
				for(let port of ports)
				{
					if(port[1] != "internal")
					{
						if(port[2] == "master") // on the right side
						{
							masters.push(port[5]);
						}
						else
						{
							slaves.push(port[5]);
						}
					}
				}
				for(let i=0; i < Math.max(masters.length, slaves.length); i++)
				{
					let left = '';
					let left_pin = "&gt;";
					if(i < slaves.length)
						left = slaves[i];
					else
						left_pin = '';
					let right = '';
					let right_pin = "&gt;";
					if(i < masters.length)
						right = masters[i];
					else
						right_pin = '';

					code_block += '\t\t<tr><td align="left" port="' + left + '">'+ left_pin + left + '</td><td align="right" port="' + right + '">' + right + right_pin + '</td></tr>\n';
				}
				code_block += "\t</table>> ];\n";
				// console.log(code_block)
				gv_file_content += code_block;
				instances_graphviz.set(key, code_block);
			}
		});

		let connection_str = "";
		if (current_document)
		{
			let conn_regexp = new RegExp("(\\w+)\.(\\w+)([\\w\\.\\[\\]]+)?\\s*=>\\s*(\\w+)\\.(\\w+)", "gms");
			let connections = text.matchAll(conn_regexp);
			for (let conn of connections)
			{
				let from = conn[1];
				let from_p = conn[2];
				let to = conn[4];
				let to_p = conn[5];
				connection_str += from + ":" + from_p + " -> " + to + ":" + to_p + "[style=solid,color=blue];\n";
			}
		}

		gv_file_content += connection_str + '}';
		let current_dir = vscode.window.activeTextEditor?.document.uri.fsPath;
		if(!current_dir)
			current_dir = ".";
		else
			current_dir = path.dirname(current_dir);
		fs.writeFileSync(path.join(current_dir, current_component+'.gv'), gv_file_content);
		function callbackFunc(webpanel: any) : void
		{
			webpanel.onClick = function(message: any) {
				// console.log("Received message - " + message.value);
			};
		}
		let args = {
			document: vscode.window.activeTextEditor?.document,
			content: gv_file_content,
			callback: callbackFunc
		};
		vscode.commands.executeCommand("interactive-graphviz.preview.beside", args);
	};
	context.subscriptions.push(vscode.commands.registerCommand(command_visualize, visualize_handler));

	// support for sgrepo files
	let provider_sgrepo = vscode.languages.registerCompletionItemProvider(
		'sgrepo',
		{
			provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {
				// Simple completion items for keywords
				let keywords = ["component", "path", "files"];
				let simpleCompletion_sgrepo = [];
				for (let keyword of keywords) {
					simpleCompletion_sgrepo.push(new vscode.CompletionItem(keyword));
				}

				const commandCompletion_component = new vscode.CompletionItem('component');
				commandCompletion_component.kind = vscode.CompletionItemKind.Keyword;
				commandCompletion_component.insertText = 'component ""\n{\n    path = \"\";\n}';
				simpleCompletion_sgrepo.push(commandCompletion_component);

				const commandCompletion_files = new vscode.CompletionItem('files');
				commandCompletion_files.kind = vscode.CompletionItemKind.Keyword;
				commandCompletion_files.insertText = 'files\n{\n    path = \"\";\n}';
				simpleCompletion_sgrepo.push(commandCompletion_files);

				let linePrefix = document.lineAt(position).text.substr(0, position.character);
				if (linePrefix.endsWith('path = \"')) {
					let file_paths = [];
					for(let lisa of lisa_files)
					{
						file_paths.push(new vscode.CompletionItem(lisa.path, vscode.CompletionItemKind.File));
					}
					return file_paths;
				}

				return simpleCompletion_sgrepo;
			}
		},
		"\""
	);

	context.subscriptions.push(provider_sgrepo);

	let provider_definitions = vscode.languages.registerDefinitionProvider(
		'lisa', new LISADefinitionProvider());
	context.subscriptions.push(provider_definitions);

	vscode.commands.executeCommand("lisa.scan");
}

export function deactivate() {}
