# Language support for LISA+ for Visual Studio Code

## Introduction

[LISA](https://en.wikipedia.org/wiki/LISA_(Language_for_Instruction_Set_Architecture)) stands for Language for Instruction Set Architecture, originated from RWTH Aachen University, in Germany. LISA+ is an enhanced version of LISA with capability of describing components and systems. The extesion is based on the published "LISA+ Language for Fast Models Reference Manual", which you can download from [here](https://static.docs.arm.com/dui0839/k/DUI0839K_lisa_rm.pdf).

The extension provides the essential LISA+ language support, such as syntax highlighting, IntelliSense. Also, the basic support for sgrepo and sgproj files are implemented to facilitate the LISA+ based models development.

## Dependencies

1. sudo apt-get install git node-typescript npm nodejs-dev node-gyp libssl1.0-dev
2. sudo npm install -g yo generator-code vsce yarn

* If nodejs 10.0 is failing to install try the following:
  1. curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  2. sudo apt-get install nodejs

## Build the extension

1. cd <path-to-repo>/lisa-vsc-extension/
2. npm install #optional
3. vsce package

## Install the extension

On VSCode, go on View → Extensions and click on the "..." → Install from VSIX... Then find the bundled .vsix extension pack under <path-to-repo>/lisa-vsc-extension/lisa-lang-0.0.9.vsix.

## Quick Start

1. Install the extension from marketplace or vsix file.
2. Open the workspace. Or open the working directory and create a new workspace.
3. If .lisa/.sgrepo/.sgproj files are opened, *LISA+: Scan Workspace* will be triggered. The command can also be triggered at any time manually.
4. Once the scan is finished, enjoy the coding!

## Features

* Scan Workspace

   The extension provides the command "LISA+: Scan Workspace" to scan the workspace and build database to offer better IntelliSense results.
   It runs automatically when the extension is activated. You can also trigger the command by pressing <kbd>shift</kbd> + <kbd>command</kbd> + <kbd>P</kbd>, then type "LISA+: Scan Workspace"

* LISA+: Visualize Current Component

   You can also visualize the current component by using the command. It analyzes the active LISA+ file, generates DOT (.gv) file and visualizes the system in Visual Studio Code.
   You can also trigger the command by pressing <kbd>shift</kbd> + <kbd>command</kbd> + <kbd>P</kbd>, then type "LISA+: Visualize Current Component"

* Syntax highlighting

   The extension supports syntax highlighting for .lisa, .sgproj and .sgrepo files.

* IntelliSence

   The extension provide IntelliSense support for .lisa, .sgproj and .sgrepo files. The list of supported IntelliSense features, but not limited to, is shown below:

   1. Code snippets for resources, ports, sections etc.
   1. Intelligent code completion for compositions, ports, keywords etc.
   1. Support of "Go to Definition" for components and protocols

## Known Issues

* The visualisation consumes large amount of memory for large and complex system models.

## License

* Inbound license:  BSD-3-Clause AND DCO-1.1
* Outbound license: BSD-3-Clause

## Release Notes

Please see [Change Log](https://github.com/ARM-software/lisa-vsc-extension/blob/master/CHANGELOG.md)
